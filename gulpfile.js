var gulp = require('gulp'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    minify = require('gulp-cssnano');

gulp.task('sass', function () {
    return gulp.src('./static/stylesheet/style.scss')
        .pipe(sass().on('error',sass.logError))
//        .pipe(minify())
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest('./static/stylesheet'));
});

gulp.task('cp', function () {
    return gulp.src('./node_modules/font-awesome/**/*.{min.css,otf,eot,svg,ttf,woff,woff2}')
        .pipe(gulp.dest('./static/font-awesome'));
});

gulp.task('pygments', function () {
    return gulp.src(['./static/pygments/*.css', '!./static/pygments/*min.css'])
        .pipe(minify())
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(gulp.dest('./static/pygments'));
});

gulp.task('watch', function() {
    gulp.watch('./static/stylesheet/*.scss', ['default']);
});

gulp.task('default', ['sass', 'cp', 'pygments']);

